worker_processes 2 # amount of unicorn workers to spin up
timeout 30
preload_app true

@dj_pid = nil
before_fork do |server, worker|
  @dj_pid ||= spawn('bundle exec rake jobs:work')
end
