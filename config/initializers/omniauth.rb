Rails.application.config.middleware.use OmniAuth::Builder do
  # The following is for facebook
  provider :facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_SECRET'],  scope: 'email, public_profile '
  # If you want to also configure for additional login services, they would be configured here.
end
