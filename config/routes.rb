
Teamsubs::Application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: 'home#index'
  devise_for :users
  match '/auth/facebook/' => 'authentications#facebook', :as => 'facebook_connect', via: [:get, :post]

  resources :users, only: [:show, :index]

  resources :events do
    member do
      get :duplicate_for_next_week
      get :resend_invitations
      get :new_email
      put :deliver_email
      get :show_subs
      get :show_same_day_subs
    end
    collection do
      get :deliver_thank_yous
    end
    resources :invitations, only: [:create]
  end

  resources :sports, only: [] do
    member do
      get :locations
    end
  end

  resources :locations, only: [:new, :create, :show]
  resources :invitations, only: [:show, :edit, :update] do
    member do
      get :open_tracker
    end
  end

  match 'invite/:id/:token(/:response)' => 'invitations#respond_from_email', :as => 'invite_with_token', via: [:get, :post]
  match '/auth/:provider/callback' => 'authentications#create', via: [:get, :post]
  post 'sendgrid/events_callback' => 'sendgrid#events_callback'

  # LEGACY STUFF
  match 'invitation/:invite_hash' => 'invitations#legacy_respond_from_email', via: [:get, :post]
  match 'respondfromemail/:invite_hash' => 'invitations#legacy_respond_from_email', via: [:get, :post]
  match 'copy/:invite_hash' => 'events#legacy_duplicate', via: [:get, :post]

  # render nothing routes for devices that just won't quit requesting garbage routes
  [ 'wp-trackback.php',
    'apple-touch-icon-precomposed.png',
    'customer/account/forgotpassword',
    'forum'
  ].each do |garbage_path|
    match garbage_path => 'home#not_found', via: [:post, :get]
  end
end
