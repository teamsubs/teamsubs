root = exports ? this

$ ->
  $('[name="commit"]').click ->
    @value = "Please wait"

  # when the form is submitted remotely, check the html of the form for extra instructions
  $("form")
    .bind "ajax:success", (event, data) ->
      window.location.reload()  if $(this).is(".reload")

  $.datepicker.setDefaults({ dateFormat: 'yy-mm-dd' })
  $('input.ui-datetime-picker').datetimepicker
    controlType: 'select',
    timeFormat: 'hh:mm tt'

  $('.btn-group').button()
  $('.btn-group').find('input[name="invitation[response]"]:checked').parent().addClass('active')

root.hideThis = ($el) ->
  $el.hide()
