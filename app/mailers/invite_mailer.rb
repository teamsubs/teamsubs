class InviteMailer < ActionMailer::Base
  helper :application
  default from: 'Teamsubs <noreply@teamsubs.com>'

  def invite(invitation)
    @invitation = invitation
    # load up the user and event in case they weren't already in memory
    @user = invitation.user
    @event = invitation.event
    mail(to: "#{@user.name} <#{@user.email}>",
         subject: "Teamsubs: #{@event.title}",
         "X-MC-Metadata" => { invitation_id: invitation.id } )
  end

  def sub_invite(invitation)
    @invitation = invitation
    # load up the user and event in case they weren't already in memory
    @user = invitation.user
    @event = invitation.event
    mail(to: "#{@user.name} <#{@user.email}>",
         subject: "Teamsubs Sub Request: #{@event.title}",
         "X-MC-Metadata" => { invitation_id: invitation.id } )
  end
  def thank_you(event)
    @event = event
    @user = event.planner_user
    mail(to: "#{@user.name} <#{@user.email}>", subject: "Plan game after #{event.title}")
  end

  def user_message(user, sender, event, body)
    @user = user
    @sender = sender
    @event = event
    @body = body
    @invitation = Invitation.where(user_id: @user.id, event_id: @event.id).first
    mail(to: "#{@user.name} <#{@user.email}>", subject: "Message about #{event.title}")
  end
end
