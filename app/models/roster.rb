class Roster < ActiveRecord::Base
  belongs_to :event
  has_many :roster_users
  has_many :users, through: :roster_users
  has_many :invitations

  accepts_nested_attributes_for :users

  def comma_separated_emails=(emails)
    # create/set users from a list
    emails.split(',').each do |email|
      user = User.where(
        email: User.new(email: email.strip).email
      ).first_or_initialize

      if user.new_record?
        password = Devise.friendly_token.first(6)
        user = User.create(
          email: email.strip,
          password: password,
          password_confirmation: password
        )
      end

      if user.persisted?
        # build if the event doesn't exist yet, because it will save this roster_user
        if event.nil?
          roster_users.build(user_id: user.id) unless users.include?(user)
        else
          roster_users.create(user_id: user.id) unless users.include?(user)
        end
      else
        logger.info("can't save user")
      end
    end
  end

  def comma_separated_emails
    # return a list of users
    users.map(&:email).join(',')
  end

  def invite_all
    users.find_each do |user|
      # create an invitation if it doesn't already exist
      Invitation.where(user_id: user.id, event_id: event.id).first_or_create
    end
  end

  def all_invited?
    invitations.count == users.count
  end

  def invite_next
    # return the user if they were invited
    # return nil if no one was invited
    users.each do |user|
      unless user.invited_to? event
        Invitation.create(user: user, event: event)
        return user
      end
    end
    nil
  end
end
