class RosterUser < ActiveRecord::Base
  belongs_to :roster
  belongs_to :user
end
