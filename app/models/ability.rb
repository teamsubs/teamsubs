class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities

    user ||= User.new # guest user (not logged in)
    can :manage, :all if user.respond_to?(:admin) && user.admin?

    # if the user if the planner of the current event
    can :manage, [Event] do |event|
      event.planner_user == user
    end
    can [:create, :update, :read], Invitation do |invitation|
      invitation.event.planner_user == user || invitation.event.users.include?(user)
    end

    # if the user is modifying their own thing
    can :manage, :all do |resource|
      if resource.respond_to?(:user)
        user == resource.user
      elsif resource.is_a?(User) && resource == user
        true
      else
        false
      end
    end

    # everyone can read events they were invited to
    can [:read, :show_same_day_subs, :new_email, :show_subs, :deliver_email], [Event] do |event|
      event.users.include?(user) || event.planner_user == user
    end

    can [:read], [Invitation] do |invitation|
      invitation.event.users.include?(user) || invitation.event.planner_user == user
    end

    # everyone can create a new Event
    can :create, Event
    can [:respond_from_email, :open_tracker], Invitation
    can :manage, [Sport, Location]
  end
end
