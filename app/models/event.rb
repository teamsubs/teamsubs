class Event < ActiveRecord::Base
  # event times are all in "local to user time zone".  Stored as UTC in the db.  Display the times as they are stored,
  # there is no need to calculate time zone.
  acts_as_cached(version: 1, expires_in: 10.days)

  has_many :rosters
  has_many :invitations
  has_many :users, through: :invitations
  belongs_to :location
  has_one :sport, through: :location
  belongs_to :planner_user, class_name: 'User'

  # make the code more readable
  attr_accessor :no_invitations, :yes_invitations, :maybe_invitations, :not_yet_replied_invitations, :email_body

  validates_presence_of :start_time, :title, :description, :location

  delegate :sport, to: :location

  accepts_nested_attributes_for :rosters

  after_save :invite_players

  scope :upcoming, -> { where('`start_time` >= ?', Time.now) }
  scope :previous, -> { where('`start_time` < ?', Time.now) }

  def invite_players
    rosters.where(primary: true).each do |roster|
      roster.invite_all
    end
  end

  def invite_next(roster)
    roster.invite_one unless roster.all_invited?
  end

  def build_duplicate
    new_event = dup
    rosters.each do |roster|
      new_roster = roster.dup
      new_roster.users << roster.users
      new_event.rosters <<  new_roster
    end
    new_event
  end

  def deliver_thank_you
    return if thank_you_sent?
    InviteMailer.delay.thank_you(self)
    update_attributes(thank_you_sent: true)
  end

  def weather
    return nil unless location.has_latlon?
    cache_key = "event_#{id}_weather"
    Rails.cache.fetch(cache_key, expires_in: 1.day) do
      ForecastIO.forecast(location.latitude, location.longitude, time: zoneless_formatted_start_time)
    end
  end

  def possible_subs
    # used for finding other games to get subs from.
    # Get users who will be playing the same sport at the same place, at a similar time
    other_events = Event.where(location_id: location.id)
      .where('`start_time` > ?', start_time - 4.hours)
      .where('`start_time` < ?', start_time + 4.hours)
      .where('`start_time` <> ?', start_time)
      .where('`events`.`id` <> ?', id)

    invitations = other_events.map { |e| e.invitations.where(response: Invitation.yes) }
    # remove currently invited people to this event and those who don't want sub requests
    users = invitations.flatten.map { |i| i.user if i.user.allow_sub_requests? && !(self.users.include?(i.user)) }
    users = users.reject { |u| u.nil? }
  end

  def zoneless_formatted_start_time
    start_time.strftime '%Y-%m-%dT%H:%M:%S'
  end

  def sport_id
    sport ? sport.id : nil
  end
end
