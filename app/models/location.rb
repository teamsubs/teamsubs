class Location < ActiveRecord::Base
  acts_as_cached(version: 1, expires_in: 1.month)
  belongs_to :sport

  validates_presence_of :address, :city, :name, :sport_id, :state, :zip
  validates :name, uniqueness: { scope: :sport_id }
  after_validation :geocode, if: ->(obj) { obj.full_address_changed? }
  geocoded_by :full_street_address   # can also be an IP address
  reverse_geocoded_by :latitude, :longitude, address: :location

  def to_s
    "#{name} #{address} #{city} #{state} #{zip}"
  end

  def full_street_address
    "#{address} #{city}, #{state} #{zip}"
  end

  def full_address_changed?
    address_changed? || city_changed? || zip_changed? || state_changed?
  end

  # geocoding libary wants a has_latlon? method
  def has_latlon? # rubocop:disable PredicateName:NamePrefixBlacklist:
    latitude.present? && longitude.present?
  end

  def guess_zip
    addr = reverse_geocode
    matches = addr.match(/(\d+\d+\d+\d+\d+)/)
    self.zip = matches[1] if matches && matches[1]
  end
end
