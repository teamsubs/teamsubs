class User < ActiveRecord::Base
  require 'digest/md5'
  require Rails.root.join('lib', 'devise', 'encryptors', 'sha1')
  acts_as_cached(version: 1, expires_in: 10.days)

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :authentications, dependent: :delete_all
  has_many :facebook_authentications, -> { where provider: 'facebook' }, class_name: 'Authentication'
  has_many :invitations
  has_many :events, through: :invitations
  has_many :roster_users
  has_many :rosters, through: :roster_users

  after_initialize do
    if new_record? && email.present?
      matches = email.match(/(.*)<(.*)>/)
      if matches.present?
        self.email = matches[2].strip.gsub('"', '').gsub("'", '')
        self.name = matches[1].strip.gsub('"', '').gsub("'", '') if matches[1].present?
      end
    end
  end

  def apply_omniauth(auth)
    # In previous omniauth, 'user_info' was used in place of 'raw_info'
    self.email = auth['info']['email'] unless email.present?
    self.name = auth['info']['name']
    # Again, saving token is optional. If you haven't created the column in authentications table, this will fail
    authentications.build(provider: auth['provider'], uid: auth['uid'], token: auth['credentials']['token'])
  end

  ## Facebook methods ##
  def facebook_authentication
    facebook_authentications.first
  end

  def connected_to_facebook?
    facebook_authentication.present?
  end

  def avatar_url(width = 200, height = 200)
    if use_gravatar?
      return "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(email.downcase)}?s=180"
    elsif connected_to_facebook?
      return "https://graph.facebook.com/#{facebook_authentication.uid}/picture?width=#{width}&height=#{height}"
    end
    nil
  end

  def display_name
    name? ? name : email.gsub(/@.+/, '')
  end

  def password=(new_password)
    @password = new_password
    self.encrypted_password = Devise::Encryptors::SHA1.digest(@password, nil, nil, nil) if @password.present?
  end

  def valid_password?(password)
    return false if encrypted_password.blank?
    Devise.secure_compare(Devise::Encryptors::SHA1.digest(password, nil, nil, nil), encrypted_password)
  end
end
