class Invitation < ActiveRecord::Base
  acts_as_cached(version: 1, expires_in: 10.days)

  belongs_to :user
  belongs_to :event

  after_create :deliver

  validates_presence_of :user, :event
  attr_accessor :sub_request

  def deliver
    if sub_request
      InviteMailer.delay.sub_invite(self)
    else
      InviteMailer.delay.invite(self)
    end
  end

  def generate_token
    Digest::SHA1.hexdigest("#{user.id}-#{id}-#{INVITATION_SALT}")[0..4]
  end

  def mark_read
    self.first_viewed_at = Time.now if first_viewed_at.nil?
    save!
  end

  def respond(response, comment = nil)
    self.response = response
    self.comment = comment if comment.present?
    save!
  end

  def self.yes
    1
  end

  def self.no
    2
  end

  def self.maybe
    3
  end

  def label(val)
    'yes' if val == 1
    'no' if val == 2
    'maybe' if val == 3
    ''
  end
end
