class LocationsController < InheritedResources::Base
  load_and_authorize_resource

  def permitted_params
    params.permit(location: [
      :sport_id, :name, :address, :city, :state, :zip
    ])
  end
end
