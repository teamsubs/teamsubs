class EventsController < InheritedResources::Base
  load_and_authorize_resource
  before_filter :authenticate_user!, except: [:deliver_thank_yous]

  def new
    # include the nested resources
    @event = Event.new
    new_roster = @event.rosters.build(primary: true)
    new_roster.users << current_user
    new!
  end

  def create
    @event = Event.new(permitted_params[:event].merge(
       planner_user_id: current_user.id
    ).permit!)
    create!
  end

  def index
    @events = current_user.events.includes(:location).order('`id` DESC').page(params[:page])
    index!
  end

  def edit
    # assure that there is at least one roster
    @event = Event.find(params[:id])
    @event.rosters.build(primary: true) if @event.rosters.empty?
    edit!
  end

  def show
    @event = Event.find(params[:id])
    load_responses
    @current_user_invitation = @all_invitations.select { |i| i.user == current_user }.first
    @current_user_invitation.mark_read if @current_user_invitation
    show!
  end

  def duplicate_for_next_week
    # make another event with the same everything a week after this
    # send to the edit view for confirmation
    new_event = @event.build_duplicate
    new_event.start_time = @event.start_time + 1.week
    @event = new_event
    render :edit
  end

  def resend_invitations
    @event.invitations.find_each do |invite|
      invite.deliver
    end
    redirect_to @event
  end

  # send an email to users -- show user picker
  def new_email
    @users = @event.users
  end

  # send an email to users -- send the email
  def deliver_email
    users = []
    params[:event][:user_ids].each do |user_id|
      users << User.find_by_id(user_id) if user_id.present?
    end
    users.each do |user|
      InviteMailer.delay.user_message(user, current_user, @event, params[:event][:email_body])
    end
    redirect_to @event
  end

  # show this event's sub list
  def show_subs
    all_possible_users = []
    @event.rosters.each do |roster|
      all_possible_users = all_possible_users + roster.users
    end
    # create a dummy invitation to allow simpleform to make compact code
    @invitation = Invitation.new
    @users = all_possible_users - @event.users
  end

  # show possible subs from other 'nearby' games
  def show_same_day_subs
    @users = @event.possible_subs
  end

  # legacy duplicate
  def legacy_duplicate
    invitation = Invitation.find_by_responsehash(params[:invite_hash])
    event = invitation.event
    redirect_to duplicate_for_next_week_event_path(event)
  end

  ## Collection actions ########################

  def deliver_thank_yous
    # send a note for all events that ended a day ago to the planner -- use the 5 hours offset because the server is UTC
    @events = Event.where('`start_time` > ?', 2.days.ago).where('`start_time` < ?', 5.hours.ago)
    @events.each do |event|
      event.deliver_thank_you
    end
    # display a page with the events in text suitable for emailing
    render layout: false
  end

  def permitted_params
    params.permit(event: [
      :title, :start_time, :planner_user_id, :userlist, :created_at,
      :updated_at, :responsevalue, :repsonsetext, :description, :location_id,
      :image, :reminder_sent, :thank_you_sent, :team_id, :league_id,
      :schedule_link, rosters_attributes: [:id, :comma_separated_emails, :primary]
    ])
  end

  private

  def load_responses
    # minimize db calls, do logic in ruby for faster processing
    @all_invitations = @event.invitations.includes(user: :facebook_authentications)
    @event.yes_invitations = @all_invitations.select { |i| i.response == Invitation.yes }
    @event.no_invitations = @all_invitations.select { |i| i.response == Invitation.no }
    @event.maybe_invitations = @all_invitations.select { |i| i.response == Invitation.maybe }
    @event.not_yet_replied_invitations = @all_invitations.select { |i| i.response.nil? }
  end
end
