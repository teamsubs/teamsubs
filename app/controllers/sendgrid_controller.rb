class SendgridController < ApplicationController
  def events_callback
    # http://docs.sendgrid.com/documentation/api/event-api/
    params[:_json].each do |event|
      # skip callbacks for events that aren't related to an invitation
      next unless (invitation = Invitation.where(id: event[:invitation_id]).first)
      case event[:event]
      when 'opened'
        invitation.mark_read
      when 'processed'
        invitation.update_attribute(:processed_at, Time.now)
      when 'dropped' || 'spamereport' || 'bounce'
        invitation.update_attribute(:not_sent_reason, params[:event])
      end
    end
    # return a 200 to let sendgrid know that we got the event
    render nothing: true
  end
end
