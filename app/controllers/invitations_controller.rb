class InvitationsController < InheritedResources::Base
  load_and_authorize_resource
  skip_authorize_resource :only => :create
  before_filter :authenticate_user!, only: [:create]

  def open_tracker
    @invitation.mark_read
    send_data(Base64.decode64(
      'R0lGODlhAQABAPAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='
    ), type: 'image/gif', disposition: 'inline')
  end

  def legacy_respond_from_email
    invitation = Invitation.find_by_responsehash(params[:invite_hash])
    invitation.respond(params[:response]) if params[:response]
    sign_out current_user if current_user
    sign_in invitation.user
    redirect_to invitation.event
  end

  # a response from email -- authenticated by a token
  def respond_from_email
    invitation = Invitation.find_by_id params[:id]
    render 'bad_token' unless invitation && params[:token] == invitation.generate_token

    invitation.respond(params[:response]) if params[:response]
    sign_out current_user if current_user
    sign_in invitation.user
    redirect_to invitation.event
  end

  def update
    super do |format|
      format.html { redirect_to @invitation.event }
    end
  end

  def create
    @invitation = Invitation.create!(params[:invitation].permit(:user_id, :event_id, :sub_request))
    render json: { status: :ok }
  end

  def permitted_params
    params.permit(invitation: [:response, :comment, :first_viewed_at, :sub_request])
  end
end
