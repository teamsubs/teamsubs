class HomeController < ApplicationController
  def index
    if current_user
      @events = current_user.events.order('`id` DESC').page(params[:page])
      render 'events/index'
      return
    end
  end

  def not_found
    render nothing: true, status: 404
  end
end
