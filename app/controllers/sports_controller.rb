class SportsController < InheritedResources::Base
  load_and_authorize_resource

  def locations
    @locations = @sport.locations
  end
end
