module ApplicationHelper
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to(name, 'removeFields(this)')
  end

  def link_to_add_fields(name, f, association, params_hash = {})
    new_object = f.object.class.reflect_on_association(association).klass.new(params_hash)
    fields = f.fields_for(association, new_object, child_index: "new_#{association}") do |builder|
      render(association.to_s.singularize + '_fields', f: builder)
    end
    link_to(name, "addFields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end

  def invite_with_token(invitation, response = nil)
    invite_with_token_url(id: invitation.id, token: invitation.generate_token, response: response)
  end

  def avatar(user, classes)
    if user.avatar_url
      image_tag user.avatar_url, class: classes
    elsif can? :edit, user
      link_to image_tag('get-a-photo.png', class: classes), facebook_connect_path
    else
      image_tag('generic-avatar.png', class: classes)
    end
  end

  def button_link_to(name, url, button_type = 'info', html_options = {})
    # create the bootstrap button default options
    options = {
      class: "btn btn-#{button_type}"
    }.merge(html_options)
    link_to name, url, options
  end

  def icon_button_link_to(name, url, icon, button_type = 'info', html_options = {})
    # create the bootstrap button default options
    options = {
      class: "btn btn-#{button_type}"
    }.merge(html_options)

    link_to url, options do
      content_tag(:i, '', class: "icon-#{icon}").html_safe + ' ' + name
    end
  end
end
