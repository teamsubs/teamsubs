module EventHelper
  def google_calendar_url(event)
    URI::HTTPS.build(
      host: 'www.google.com',
      path: '/calendar/b/0/render',
      query: ({
        action: 'TEMPLATE',
        text: event.title,
        dates: "#{event.start_time.strftime('%Y%m%dT%H%M00')}/#{(event.start_time + 60.minutes)
                .strftime('%Y%m%dT%H%M00')}",
        details: '',
        location: "#{event.location.address}, #{event.location.city} #{event.location.state}",
        trp: 'false',
        description: event.description,
        pli: '1',
        sf: 'true',
        output: 'xml'
      }).to_query
    ).to_s
  end

  def static_map_url(location, map_size = '300x300', zoom = 13)
    url = "http://maps.googleapis.com/maps/api/staticmap?sensor=true&markers=color:blue|#{location.to_s}"
    url << "&size=#{map_size}&zoom=#{zoom}"
  end
end
