source 'https://rubygems.org'
ruby "2.1.9"

gem 'rails', '~>4.1'
gem 'mysql2'
gem 'unicorn'
gem 'unicorn-rails'

group :assets do
  gem 'sass-rails',   '~> 4.0.3'
  gem 'coffee-rails', '~> 4.0.0'
  gem 'uglifier',     '>= 1.3.0'
end
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'turbolinks'

gem "bootstrap-sass",  '~> 3.0.0.0.rc'
gem "devise", ">= 2.1.2"
gem 'devise-encryptable'
gem "cancan", ">= 1.6.10"
gem "newrelic_rpm"
gem 'bourbon'
gem 'koala'
gem 'haml'
gem 'kaminari'
gem 'ui_datepicker-rails3'
gem 'inherited_resources'
gem 'roadie'
gem 'nokogiri'
gem 'zocial-rails'
gem 'dalli'
gem "second_level_cache", git: 'https://github.com/hooopo/second_level_cache'
gem 'responders'
gem 'simple_form'
gem 'delayed_job_active_record'
gem 'memcachier'
gem 'rollbar'
gem 'geocoder'
gem 'forecast_io'
gem 'omniauth'
gem 'omniauth-facebook'
gem 'oauth2'
gem 'dotenv-rails'
gem 'rails_admin'
gem 'nested_form'

# test only
group :test do
  gem "capybara"
  gem "email_spec"
  gem "database_cleaner"
  gem 'simplecov', :require => false
  gem 'vcr'
  gem 'therubyracer'
  gem 'webmock'
end

# development and test
group :test, :development do
  gem "rspec-rails"
  gem "launchy"
  gem "factory_girl_rails"
  gem 'pry'
  gem 'guard'
  gem 'guard-rubocop'
  gem 'guard-rspec'
end

# development only
group :development do
  gem 'letter_opener'
  gem 'rubocop'
  gem 'binding_of_caller'
  gem 'better_errors'
  gem 'brakeman', require: false
  gem 'spring'
end

group :production do
  gem 'rails_12factor'
end