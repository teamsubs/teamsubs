# lib/devise/encryptors/SHA1.rb
require 'digest/sha1'

module Devise
  module Encryptable
    module Encryptors
      class Devise::Encryptors::SHA1 < Base
        def self.digest(password, stretches, salt, pepper)
          Digest::SHA1.hexdigest("change-me--#{password}--")
        end
      end
    end
  end
end
