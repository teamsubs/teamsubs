class DeviseCreateUsers < ActiveRecord::Migration
  def change
    rename_column :users, :password, :encrypted_password

    ## Recoverable
    rename_column :users, :reset_password_code, :reset_password_token
    remove_column :users, :reset_password_code_until
    add_column :users, :reset_password_sent_at, :datetime

    ## Rememberable
    add_column :users, :remember_created_at, :datetime

    ## Trackable
    add_column :users, :sign_in_count, :integer, default: 0
    add_column :users, :current_sign_in_at, :datetime
    add_column :users, :last_sign_in_at, :datetime
    add_column :users, :current_sign_in_ip, :string
    add_column :users, :last_sign_in_ip, :string

    ## Confirmable
    # t.string   :confirmation_token
    # add_column :users, :confirmed_at
    # add_column :users, :confirmation_sent_at
    # t.string   :unconfirmed_email # Only if using reconfirmable

    ## Lockable
    # t.integer  :failed_attempts, :default => 0 # Only if lock strategy is :failed_attempts
    # t.string   :unlock_token # Only if unlock strategy is :email or :both
    # add_column :users, :locked_at

    ## Token authenticatable
    # t.string :authentication_token

    add_column :users, :time_zone, :string

    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
    # add_index :users, :confirmation_token,   :unique => true
    # add_index :users, :unlock_token,         :unique => true
    # add_index :users, :authentication_token, :unique => true
  end
end
