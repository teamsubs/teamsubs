class AddGravatar < ActiveRecord::Migration
  def change
    add_column :users, :use_gravatar, :boolean, default: false
  end
end
