class CreateRosters < ActiveRecord::Migration
  def up
    rename_table :userlists, :rosters
    add_column :rosters, :primary, :boolean
    rename_column :rosters, :min_wanted, :minimum
    rename_column :rosters, :next_userlist_id, :next_roster_id

    Roster.find_each do |roster|
      count = Roster.count
      puts "updating #{roster.id} or #{count}"
      roster.update_attribute(:primary, true) if roster.next_roster_id.present?
    end

    add_index :rosters, :event_id
  end
end
