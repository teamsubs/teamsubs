class CreateRosterUsers < ActiveRecord::Migration
  def change
    rename_table :userjoinuserlists, :roster_users
    rename_column :roster_users, :userlist_id, :roster_id
    add_index :roster_users, :roster_id
    add_index :roster_users, :user_id
  end
end
