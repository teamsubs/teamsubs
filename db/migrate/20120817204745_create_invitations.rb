class CreateInvitations < ActiveRecord::Migration
  def change
    rename_table :userevents, :invitations

    rename_column :invitations, :responsestring, :comment
    rename_column :invitations, :first_viewed, :first_viewed_at
    add_column :invitations, :processed_at, :datetime
    add_column :invitations, :dropped_at, :datetime
    add_column :invitations, :delivered_at, :datetime
    add_column :invitations, :deferred_at, :datetime
    add_column :invitations, :bounce_at, :datetime
    add_column :invitations, :open_at, :datetime
    add_column :invitations, :sent_at, :datetime
    add_column :invitations, :spamreport_at, :datetime
    add_column :invitations, :unsubscribe_at, :datetime
    add_column :invitations, :not_sent_reason, :string

    add_index :invitations, :user_id
    add_index :invitations, :event_id
  end
end
