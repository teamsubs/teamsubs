class CreateEvents < ActiveRecord::Migration
  def change
    rename_column :events, :name, :title
    rename_column :events, :starttime, :start_time
    rename_column :events, :planner, :planner_user_id
  end
end
