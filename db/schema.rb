# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140105161420) do

  create_table "adevents", force: true do |t|
    t.integer  "ad_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "adlocations", force: true do |t|
    t.integer  "ad_id"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ads", force: true do |t|
    t.integer  "number_purchased"
    t.string   "image_url"
    t.string   "link_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "number_not_shown"
    t.string   "image_alt"
    t.string   "text"
    t.string   "name"
  end

  create_table "authentications", force: true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "availabletimes", force: true do |t|
    t.integer  "day"
    t.integer  "location_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ctaalarms", force: true do |t|
    t.string   "url"
    t.integer  "minute"
    t.string   "twitter_id"
    t.integer  "first_check"
    t.string   "to_skip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0
    t.integer  "attempts",   default: 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "designs", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emails", force: true do |t|
    t.string   "from"
    t.string   "to"
    t.integer  "last_send_attempt", default: 0
    t.text     "mail"
    t.datetime "created_on"
  end

  create_table "events", force: true do |t|
    t.string   "title"
    t.datetime "start_time"
    t.integer  "planner_user_id"
    t.string   "userlist"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "responsevalue"
    t.string   "repsonsetext"
    t.text     "description"
    t.integer  "location_id"
    t.string   "image"
    t.integer  "reminder_sent"
    t.integer  "thank_you_sent"
    t.integer  "team_id"
    t.integer  "league_id"
    t.string   "schedule_link"
  end

  create_table "invitations", force: true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "response"
    t.string   "comment",         limit: 800
    t.string   "responsehash"
    t.datetime "first_viewed_at"
    t.datetime "processed_at"
    t.datetime "dropped_at"
    t.datetime "delivered_at"
    t.datetime "deferred_at"
    t.datetime "bounce_at"
    t.datetime "open_at"
    t.datetime "sent_at"
    t.datetime "spamreport_at"
    t.datetime "unsubscribe_at"
    t.string   "not_sent_reason"
  end

  add_index "invitations", ["event_id"], name: "index_invitations_on_event_id", using: :btree
  add_index "invitations", ["user_id"], name: "index_invitations_on_user_id", using: :btree

  create_table "locations", force: true do |t|
    t.string  "name"
    t.string  "address"
    t.string  "city"
    t.string  "state"
    t.string  "zip"
    t.integer "sport_id"
    t.float   "latitude"
    t.float   "longitude"
  end

  create_table "responses", force: true do |t|
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roster_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "roster_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roster_users", ["roster_id"], name: "index_roster_users_on_roster_id", using: :btree
  add_index "roster_users", ["user_id"], name: "index_roster_users_on_user_id", using: :btree

  create_table "rosters", force: true do |t|
    t.integer  "minimum"
    t.integer  "next_roster_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_id"
    t.string   "name",             limit: 11
    t.integer  "prev_userlist_id"
    t.string   "responsehash"
    t.boolean  "primary"
  end

  add_index "rosters", ["event_id"], name: "index_rosters_on_event_id", using: :btree

  create_table "sports", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sex"
    t.string   "encrypted_password",     limit: 40
    t.string   "reset_password_token"
    t.integer  "allow_sub_requests",                default: 1
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "time_zone"
    t.boolean  "admin",                             default: false
    t.boolean  "use_gravatar",                      default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
