FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "Factory#{n} User#{n}" }
    sequence(:email) { |n| "FactoryUser#{n}@teamsubs.com" }
    password 'please'
    password_confirmation 'please'
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end
end
