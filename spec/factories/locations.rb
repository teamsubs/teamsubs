# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    sequence(:name) { |n| "Factory Location#{n}" }
    address 'MyString'
    city 'MyString'
    state 'MyString'
    zip 'MyString'
    sport_id 1
  end

  factory :river_park, class: 'Location' do
     name "River Park"
     address "5000 N Albany"
     city "Chicago"
     state "Il"
     zip "60625"
     sport_id 1
   end
end
