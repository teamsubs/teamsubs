# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :roster_user do
    association :roster
    association :user
  end
end
