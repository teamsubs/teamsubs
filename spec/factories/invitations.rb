# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invitation do
    association :user
    association :event
    response nil
    comment 'MyText'
    first_viewed_at '2012-08-17 15:47:45'
  end
end
