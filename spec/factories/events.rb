# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    title 'event title'
    start_time '2012-08-17 15:45:28'
    association :location
    description 'MyText'
    association :planner_user, factory: :user
    schedule_link 'MyString'
  end
end
