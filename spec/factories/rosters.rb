# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :roster do
    next_roster_id 1
    association :event
    primary true
  end
end
