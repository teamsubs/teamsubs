require 'spec_helper'

describe InvitationsController, :vcr do
  before do
    @invitation = FactoryGirl.create(:invitation)
  end

  describe '#sendgrid_event' do

  end

  describe '#open_tracker' do

  end

  describe '#respond' do

  end

  describe '#respond_from_email' do
    before do
      get :respond_from_email, id: @invitation.id, token: @invitation.generate_token, response: Invitation.yes
    end

    it 'marks the invitation' do
      # have to reload twice.  This seems like an error somewhere
      @invitation.reload
      expect(@invitation.reload.response).to eq(Invitation.yes)
    end
  end

  describe '#update' do

  end

end
