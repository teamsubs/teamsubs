require 'spec_helper'

describe EventsController, :vcr do
  before do
    # set up an event with a roster and a user on that roster.
    @event = FactoryGirl.create(:event)
    roster = FactoryGirl.create(:roster, event: @event)
    roster_user = FactoryGirl.create(:roster_user, roster: roster)
    @user = roster_user.user
    # force the event to go through its callbacks, including creating the invitation for the user
    @event.save
    sign_in @event.planner_user
  end

  describe 'GET /' do
    it 'should be successful' do
      get :index
      response.should be_success
    end
  end

  describe 'GET :id' do
    it 'should be successful' do
      get :show, id: @event.id
      response.should be_success
    end

    it 'throws an access denied error for users not on the roster' do
      some_guy = FactoryGirl.create(:user)
      sign_in some_guy
      get :show, id: @event.id
      expect(response.location).to eq(root_url)
    end
  end

  describe 'PUT update' do
    it 'adds users to the event' do
      # add a new user email to the list
      emails = @event.users.map(&:email) << 'foo@bar.com'
      put :update,  id: @event.id, event: {
                    title: 'sunday: game 2',
                    description: '4pm this week!',
                    start_time: '2013-03-10 16:00',
                    rosters_attributes: {
                      '0' => {
                        primary: 'true',
                        comma_separated_emails: emails.join(','),
                        id: @event.rosters.first.id
                      }
                    }
                  }

      @event.reload
      expect(@event.users.where(email: 'foo@bar.com')).to_not be(nil)
    end
  end

  describe 'GET new' do
    it 'builds a new event' do
      get :new
      expect(assigns(:event)).to_not be(nil)
    end
  end

  describe 'GET duplicate_for_next_week' do
    before do
      get :duplicate_for_next_week, id: @event.id
    end

    it 'builds an event with the sport' do
      expect(assigns(:event).sport_id).to eq(@event.sport_id)
    end

    it 'builds an event with the location' do
      expect(assigns(:event).location_id).to eq(@event.location_id)
    end

    it 'builds an event with a date of one week later than the original one' do
      expect(assigns(:event).start_time).to eq(@event.start_time + 1.week)
    end

  end
end
