require 'spec_helper'

describe Event, :vcr do
  describe '#possible_subs' do
    before do
      @invitation_one = FactoryGirl.create(:invitation)
      @event_two = FactoryGirl.create(:event,
                                      start_time: @invitation_one.event.start_time + 1.hour,
                                      location: @invitation_one.event.location
      )
      @invitation_two = FactoryGirl.create(:invitation, event: @event_two, response: Invitation.yes)
    end

    it 'includes users from other games' do
      @invitation_one.event.possible_subs.should include @invitation_two.user
    end

    it 'does not include users who have opted out' do
      @invitation_two.user.update_attribute(:allow_sub_requests, 0)
      @invitation_one.event.possible_subs.should_not include @invitation_two.user
    end

    it 'does not include users from the same game' do
      @invitation_one.event.possible_subs.should_not include @invitation_one.user
    end

    it 'does not include users from a game at the same time' do
      @event_two.update_attribute(:start_time, @invitation_one.event.start_time)
      @invitation_one.event.possible_subs.should_not include @invitation_two.user
    end
  end

  describe '#build_duplicate' do
    before do
      @invitation = FactoryGirl.create(:invitation)
      @event = @invitation.event
    end

    it 'includes the location' do
      expect(@event.build_duplicate.location_id).to eq(@event.location_id)
    end
  end

  describe '#weather' do
    it 'gets the weather for an event', :vcr, match_requests_on: [:host] do
      event = FactoryGirl.create(:event, location: FactoryGirl.create(:river_park))
      event.start_time = Time.now + 1.day
      expect(event.weather.currently).to_not be(nil)
    end
  end
end
